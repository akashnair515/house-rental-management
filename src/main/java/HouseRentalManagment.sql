/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 08-Jun-2022
 */
CREATE DATABASE IF NOT EXISTS HOUSE_RENTAL_MANAGMENT;
USE HOUSE_RENTAL_MANAGMENT;
CREATE TABLE IF NOT EXISTS RENTAL_DETAILS(ROOM_N0 INT PRIMARY KEY AUTO_INCREMENT,LOCATION VARCHAR(20) NOT NULL,COST DECIMAL(8,2) NOT NULL);
ALTER TABLE RENTAL_DETAILS ADD ID_PROOF_TYPE VARCHAR(20);
ALTER TABLE RENTAL_DETAILS MODIFY COLUMN ID_PROOF_TYPE VARCHAR(20) NOT NULL;
DESC RENTAL_DETAILS;
